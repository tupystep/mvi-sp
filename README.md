
# NI-MVI – Semestrální práce

Moje semestrální práce řeší úkol zadaný v Kaggle soutěži **Optiver – Trading at the Close** (https://www.kaggle.com/competitions/optiver-trading-at-the-close), jejíž cíl je predikce pohybu uzavírací ceny stovek akcií z indexu Nasdaq na základě dat z knihy objednávek a uzavírací aukce. Konkrétně je cílem predikce rozdílu mezi pohybem weighted average price (WAP) v knize objednávek a pohybem WAP v syntetickém indexu za 60 sekund.

Data poskytnutá v soutěži jsem transformoval na časové řady a pro predikci jsem použil rekurentní neuronové sítě LSTM a GRU optimaliztované vůči MAE. Celé řešení je implementované v Jupyter Notebooku `optiver.ipynb`, ve kterém jsou vytvořené 4 různé modely. Modely jsou už natrénované a uložené ve složce `models`, takže notebook lze spustit bez časově náročného trénování.
